<?php
$whiteRabbit = new WhiteRabbit();
//$whiteRabbit->findMedianLetterInFile("")
class WhiteRabbit
{
    public function findMedianLetterInFile($filePath)
    {
        return array("letter"=>$this->findMedianLetter($this->parseFile($filePath),$occurrences),"count"=>$occurrences);
    }

    /**
     * Parse the input file for letters.
     * @param $filePath
     */
    private function parseFile ($filePath)
    {
        if(file_exists($filePath)){
            $fileString = strtolower(file_get_contents($filePath));
            return $fileString;
        }
    }

    /**
     * Return the letter whose occurrences are the median.
     * @param $parsedFile
     * @param $occurrences
     */
    private function findMedianLetter($parsedFile, &$occurrences)
    {

        $countArr = array("a"=>0, "b"=>0,"c"=>0,
            		"d"=>0,"e"=>0, "f"=>0,"g"=>0, "h"=>0,"i"=>0,
            		"j"=>0,"k"=>0, "l"=>0,"m"=>0, "n"=>0,"o"=>0,
            		"p"=>0,"q"=>0, "r"=>0,"s"=>0, "t"=>0,"u"=>0,
            		"v"=>0,"w"=>0, "x"=>0,"y"=>0, "z"=>0);

        //looping through file string, checking if it is a letter and incrementing countArr
        for($i = 0; $i < strlen($parsedFile); $i++){
            $currentChar = substr($parsedFile, $i,1 );
            $asciiOfChar = ord($currentChar);
            if($asciiOfChar>= 97 && $asciiOfChar <= 122){
                $countArr[chr($asciiOfChar)]++;
            }
        }

        //sorting and returning
        asort($countArr);
        $middle = (count($countArr)/2)-1;
        $charOccurences = array_values($countArr);
        $occurrences = $charOccurences[$middle];
        return array_search($occurrences, $countArr);



    }


}
