<?php
$whiteRabbit2 = new WhiteRabbit2();
echo $whiteRabbit2->findCashPayment($amount = 40);
class WhiteRabbit2
{
    /** r
     * return a php array, that contains the amount of each type of coin, required to fulfill the amount.
     * The returned array should use as few coins as possible.
     * The coins available for use is: 1, 2, 5, 10, 20, 50, 100
     * You can assume that $amount will be an int
     */
    public function findCashPayment($amount){
        $coinArr = array(
            '1'   => 0,
            '2'   => 0,
            '5'   => 0,
            '10'  => 0,
            '20'  => 0,
            '50'  => 0,
            '100' => 0);

        //looping until amount reaches 0, incrementing each coin, then returning the array.

        while($amount > 0){
            if($amount >= 100){
                $coinArr['100']++;
                $amount -= 100;
            } else if($amount >= 50){
                $coinArr['50']++;
                $amount -= 50;
            } else if($amount >= 20) {
                $coinArr['20']++;
                $amount -= 20;
            } else if($amount >= 10) {
                $coinArr['10']++;
                $amount -= 10;
            } else if($amount >= 20) {
                $coinArr['20']++;
                $amount -= 20;
            } else if($amount >= 10) {
                $coinArr['10']++;
                $amount -= 10;
            } else if($amount >= 5) {
                $coinArr['5']++;
                $amount -= 5;
            } else if($amount >= 2) {
                $coinArr['2']++;
                $amount -= 2;
            } else {
                $coinArr['1']++;
                $amount =0;
            }
        }
        return $coinArr;
    }
}