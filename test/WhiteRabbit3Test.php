<?php

namespace test;

require_once(__DIR__ . "/../src/WhiteRabbit3.php");

use PHPUnit_Framework_TestCase;
use WhiteRabbit3;

class WhiteRabbit3Test extends PHPUnit_Framework_TestCase
{
    /** @var WhiteRabbit3 */
    private $whiteRabbit3;

    public function setUp()
    {
        parent::setUp();
        $this->whiteRabbit3 = new WhiteRabbit3();

    }

    //SECTION FILE !
    /**
     * @dataProvider multiplyProvider
     */
    public function testMultiply($expected, $amount, $multiplier){
        $this->assertEquals($expected, $this->whiteRabbit3->multiplyBy($amount, $multiplier));
    }
    //making sure it never reaches the while loop causes it to fail
    //strings are allowed causing it to fail.
    public function multiplyProvider(){
        return array(
            array(-1, -2, -5),
            array(0.5,1,15,2),
            array(1.02, 0, 5),
            array(0,0),
            array(0),
            array(1.11, 5,123),
            array("bob", "sings", "jazz")
        );
    }
}
